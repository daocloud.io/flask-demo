from unittest import TestCase

from app import app


class Test(TestCase):
    def setUp(self):
        self.client = app.test_client()

    def test_demo(self):
        rsp = self.client.get("/")
        self.assertEqual(rsp.status_code, 200)
        self.assertIn("Hello World!", rsp.data.decode())
